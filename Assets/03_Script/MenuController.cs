﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    List<GameObject> Models = new List<GameObject>();

    float Current_model_index = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeModel(bool IsNext){
        if(IsNext){
            Current_model_index++;
        }else{
            Current_model_index--;
        }

        if(Current_model_index < 0 && !IsNext){
            Current_model_index = Models.Count -1;
        }

        if(Current_model_index >= Models.Count && IsNext){
            Current_model_index = 0;
        }

        for(int i = 0; i< Models.Count; i++){
            if(i == Current_model_index)
                Models[i].SetActive(true);
            else
                Models[i].SetActive(false);
        }
    }

    public void Play(){
        SceneManager.LoadScene(1);
    }
}
