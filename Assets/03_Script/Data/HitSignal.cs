﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GetHit_Sigal", menuName = "Data/Hit_Data", order = 1)]
public class HitSignal : ScriptableObject
{
    [SerializeField]
    float _Dmg = 10;
    public float Dmg => _Dmg;
    [SerializeField]
    Vector2 _ForceBack;
    public Vector2 ForceBack => _ForceBack;
}
