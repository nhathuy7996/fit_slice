﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Creep_Elect : MonoBehaviour
{
    [SerializeField]
    float RangeGuard = 2, RangeAtive = 1,Speed = 100;
    [SerializeField]
    float _HP = 100;
    Rigidbody2D Rigi = null;
    [SerializeField]
    bool IsImmue = false;
    Text Name_text;

    [SerializeField]
    Transform Target = null;
    [SerializeField]
    STATE _State = STATE.IDLE;

    Animator Anim = null;
    [SerializeField]
    bool IsHit, IsAtk, IsShield, IsDead;

    [SerializeField]
    float TimeShock = 3, TimeShield = 5;

    // Start is called before the first frame update
    void Start()
    {
        Anim = this.GetComponent<Animator>();
        Rigi = this.GetComponent<Rigidbody2D>();
        Name_text = GameController.Instant.Pool.GetTextNameCreep(this.transform.position,"Normal").GetComponent<Text>();
        if (IsImmue)
            Name_text.text = "Immue";
    }

    // Update is called once per frame
    void Update()
    {
        Name_text.transform.position = Camera.main.WorldToScreenPoint(this.transform.position + Vector3.up);
        ChangeState();
        ChangeAnim();
        CheckTarget();
    }

    private void FixedUpdate()
    {
        Move();
    }

    void CheckTarget()
    {
        if (Mathf.Abs(GameController.Instant.PlayerController.transform.position.x - this.transform.position.x) <= RangeGuard)
        {
            Target = GameController.Instant.PlayerController.transform;
            if(!IsShield && !IsAtk)
            {
                TimeShield = 5;
                TimeShock = 3;
                IsAtk = true;
                IsShield = true;
            }
        }
        else
            Target = null;
    }

    void Move()
    {
        if (_State != STATE.RUN)
        {
            Rigi.velocity = new Vector2(0,Rigi.velocity.y);
            return;
        }
           
        Rigi.velocity = new Vector2(Speed * Time.deltaTime * (Target.position.x > this.transform.position.x ? 1:-1) , Rigi.velocity.y);
    }

    void ChangeState()
    {

        if(Target == null)
        {
            _State = STATE.IDLE;
            return;
        }
        this.transform.localScale = new Vector2(Mathf.Abs(this.transform.localScale.x) * (Target.position.x > this.transform.position.x ? -1 : 1),
                                                this.transform.localScale.y);
        float distance = Mathf.Abs(this.transform.position.x - Target.position.x);
        if (distance > RangeAtive)
        {
            _State = STATE.RUN;
            return;
        }
        
        if (IsHit)
        {
            _State = STATE.HIT;
            return;
        }

        if (IsDead)
        {
            _State = STATE.DEAD;
            return;
        }

        if(TimeShock > 0 && IsAtk)
        {
            _State = STATE.ATK;
            TimeShock -= Time.deltaTime;
            return;
        }
        IsAtk = false;
        TimeShock = 3;

        if (TimeShield > 0 && IsShield)
        {
            _State = STATE.SHIELD;
            TimeShield -= Time.deltaTime;
            return;
        }

        IsShield = false;
        TimeShield = 5;
        
    }

    void ChangeAnim()
    {
        
        for(int i = 0; i< (int)STATE.last; i++)
        {
            if((STATE)i != _State)
                Anim.SetBool(((STATE)i).ToString(),false);
            else
                Anim.SetBool(_State.ToString(), true);
        }
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        SwordBase S = col.GetComponent<SwordBase>();
        if(S == null)
            return;
        if (!IsImmue &&  _State != STATE.SHIELD)
        {
            _HP -= S.DataHit.Dmg;
            GameController.Instant.Pool.GetTextDmg(this.transform.position,"-"+S.DataHit.Dmg).SetActive(true);
            IsHit = true;
        }
            
        Vector2 ForceBack = S.DataHit.ForceBack;
        ForceBack.x *= (this.transform.position.x > GameController.Instant.PlayerController.transform.position.x ? 1: -1);
        Rigi.AddForce(ForceBack);
        //GetHit anim
       
        if (_HP <= 0)
        {
            IsDead = true;
        }
    }

    void Dead()
    {
        GameObject blood = GameController.Instant.Pool.Blood;
        blood.transform.position = new Vector3(this.transform.position.x, blood.transform.position.y, blood.transform.position.z);
        blood.SetActive(true);
        Name_text.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    enum STATE
    {
        RUN,
        IDLE,
        ATK,
        SHIELD,
        HIT,
        DEAD,
        last
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position,RangeGuard);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, RangeAtive);
    }
}
