﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Animator AnimControl = null;
    Rigidbody2D Rigi = null;

    [SerializeField]
    float _HP = 100,SpeedRun = 100,RangeAtk = 1;
    public float HP => _HP;
    [SerializeField]
    List<HitSignal> DataHit = new List<HitSignal>();
    [SerializeField]
    SwordBase Sword;
    float TargetPos;
    [SerializeField]
    Transform _CreepTarget;
    public Transform CreepTarget => _CreepTarget;

    [SerializeField]
    private PLAYER_STATE _PlayerState = PLAYER_STATE.IDLE;
    [SerializeField]
    bool _IsMoving = false, _CanAttack = false, _IsGetHit = false;


    // Start is called before the first frame update
    void Start()
    {
        AnimControl = this.GetComponent<Animator>();
        Rigi = this.GetComponent<Rigidbody2D>();
        TargetPos = this.transform.position.x;
    }

    private void Update()
    {
        if(_PlayerState != PLAYER_STATE.DIE){
            DefineTargetPos();
            IsAttack();
            Attacking();
            ChangeState();
        }

        ChangeAnim();       
    }

    private void FixedUpdate()
    {
        Moving();
    }

    public void ListenClick(Vector2 mousePos)
    {
        _CreepTarget = GameController.Instant.Creep_Manager.CheckCreepTarget(mousePos);
        if (!_CreepTarget)
            TargetPos = mousePos.x;
    }

    void IsAttack()
    {
        if (_CreepTarget == null)
        {
            _CanAttack = false;
            return;
        }

        float distance = Mathf.Abs(this.transform.position.x - _CreepTarget.position.x);
        if (distance > RangeAtk + 0.2f)
        {
            _CanAttack = false;
            return;
        }
        
        if(_IsMoving){
            return;
        }
        _CanAttack = true;
    }

    public virtual void Attacking()
    {
        if(_IsGetHit)
            return;
        if (!_CanAttack)
            return;
        this.transform.localScale = new Vector2(Mathf.Abs(this.transform.localScale.x) * (this.transform.position.x > _CreepTarget.position.x ? -1 : 1),
                                                 this.transform.localScale.y);
        if (AnimControl.GetCurrentAnimatorStateInfo(0).IsName(PLAYER_STATE.ATK.ToString())) //This to avoid recalculate attack index
            return;

        int ran = Random.Range(0,3);
        Sword.DataHit = DataHit[ran];
        AnimControl.SetFloat("Atk_index", ran);
        _PlayerState = PLAYER_STATE.ATK;
    }

    void DefineTargetPos()
    {
        if(_IsGetHit){
            TargetPos = this.transform.position.x;
            _CreepTarget = null;
            return;
        }
        if (_CreepTarget == null)
            return;
        if (!_CreepTarget.gameObject.activeSelf)
        {
            _CreepTarget = null;
            TargetPos = this.transform.position.x;
            return;
        }
        float distance = Mathf.Abs(this.transform.position.x - _CreepTarget.position.x);
        if (distance > RangeAtk)
            TargetPos = _CreepTarget.position.x + RangeAtk * (this.transform.position.x > _CreepTarget.position.x ? 1 : -1);
        else
            TargetPos = this.transform.position.x;
    }

    void Moving()
    {
        if(_PlayerState == PLAYER_STATE.DIE)
            return;
        if(_IsGetHit)
            return;
        if(_CanAttack)
            return;
        float Look = this.transform.position.x > TargetPos ? -1 : 1;
        
        _IsMoving = Mathf.Abs(this.transform.position.x - TargetPos) > 0.1f;
        if (!_IsMoving)
        {
            Rigi.velocity = new Vector2(0, Rigi.velocity.y);
            return;
        }
        this.transform.localScale = new Vector2(Mathf.Abs(this.transform.localScale.x) * Look, this.transform.localScale.y);
        Rigi.velocity = new Vector2(SpeedRun * Time.deltaTime * Look, Rigi.velocity.y);
    }

    void ChangeState()
    {
        if(_IsGetHit){
            _PlayerState = PLAYER_STATE.HIT;
            return;
        }
        if (_IsMoving)
        {
            _PlayerState = PLAYER_STATE.RUN;
            return;
        }
        AnimControl.SetFloat("IsSword", _CreepTarget != null ? 1: 0);
        if (_CanAttack)
        {
            _PlayerState = PLAYER_STATE.ATK;
            return;
        }

        AnimControl.SetFloat("IsSword", _CreepTarget != null ? 1: 0);
        _PlayerState = PLAYER_STATE.IDLE;
    }

    void ChangeAnim()
    {
        if (AnimControl.GetCurrentAnimatorStateInfo(0).IsName(_PlayerState.ToString()))
            return;
        for (int i = 0; i < (int)PLAYER_STATE.last; i++)
        {
            if((PLAYER_STATE)i == _PlayerState)
                AnimControl.SetBool(((PLAYER_STATE)i).ToString(), true);
            else
                AnimControl.SetBool(((PLAYER_STATE)i).ToString(), false);
        }
    }

    void AttackDone()
    {
        _CanAttack = false;
    }

    void CallSworDone(){
        AnimControl.SetBool("TriggerSword", _CreepTarget != null );
    }

    public void GetHit(HitSignal HitData){
        _IsGetHit = true;
        _HP -= HitData.Dmg;
        GameController.Instant.Pool.GetTextDmg(this.transform.position,"-"+HitData.Dmg).SetActive(true);
        if(_HP <= 0){
            _PlayerState = PLAYER_STATE.DIE;
        }
    }

    void GetHitDone(){
        _IsGetHit = false;

    }

    public enum PLAYER_STATE
    {
        IDLE,
        RUN,
        ATK,
        HIT,
        DIE,
        last
    }
}
