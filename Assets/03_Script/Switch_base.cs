﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Switch_base : MonoBehaviour
{
    [SerializeField]
    bool Forward = false;
    [SerializeField]
    int IndexLevel = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(Forward){
            IndexLevel = SceneManager.GetActiveScene().buildIndex + 1;
        }else{
            IndexLevel = SceneManager.GetActiveScene().buildIndex - 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.GetComponent<PlayerController>() != null)
            GameController.Instant.LoadScene(IndexLevel);
    }
}
