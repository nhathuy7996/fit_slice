﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Creep_base : MonoBehaviour
{
    [SerializeField]
    string NameCreep = "Normal";
    [SerializeField]
    float Speed, RangeAtk,_HP = 100;
    [SerializeField]
    HitSignal HitData;
    [SerializeField]
    bool IsImmue = false;

    Rigidbody2D Rigi = null;
    Animator Anim = null;
    Text Name_text;
    [SerializeField]
    Transform Target = null;
    float TargetPos;
    [SerializeField]
    float CountDownAtk = 0;

    [Header("Cotrol State")]
    [SerializeField]
    Creep_State CREEP_STATE = Creep_State.IDLE;
    [SerializeField]
    bool _IsMoving,_IsAtk,_IsHit,_IsDead = false;


    // Start is called before the first frame update
    void Start()
    {
        Anim = this.GetComponent<Animator>();
        Rigi = this.GetComponent<Rigidbody2D>();
        Name_text = GameController.Instant.Pool.GetTextNameCreep(this.transform.position,NameCreep).GetComponent<Text>();
        if (IsImmue)
            Name_text.text = NameCreep +" Immue";
    }

    // Update is called once per frame
    void Update()
    {
        DefineTarget();
        DefineState();
        ChangeAnim();
    }

    void FixedUpdate(){
        this.Moving();
    }

    void DefineState(){
                
        if(_IsDead){
            CREEP_STATE = Creep_State.DIE;
            return;
        }
  
        if(_IsHit){
            CREEP_STATE = Creep_State.HIT;
            return;
        }

        if(_IsMoving){
            CREEP_STATE = Creep_State.RUN;
            return;
        }

        if(_IsAtk){
            CREEP_STATE = Creep_State.ATK;
            return;
        }

        CREEP_STATE = Creep_State.IDLE;
    }
      void ChangeAnim()
    {
        if (Anim.GetCurrentAnimatorStateInfo(0).IsName(CREEP_STATE.ToString()))
            return;
        for (int i = 0; i < (int)Creep_State.last; i++)
        {
            if((Creep_State)i == CREEP_STATE)
                Anim.SetBool(((Creep_State)i).ToString(), true);
            else
                Anim.SetBool(((Creep_State)i).ToString(), false);
        }
    }

    protected virtual void Moving(){
        if(_IsHit || _IsDead)
            return;
        Name_text.transform.position = Camera.main.WorldToScreenPoint(this.transform.position + Vector3.up);

        if(Mathf.Abs(this.transform.position.x - TargetPos) <= 0.1f){
            Rigi.velocity = new Vector2(0,Rigi.velocity.y);
            if(Target != null){
                _IsAtk = true;
                Attack();
            }else
                _IsMoving = false;
            return;
        }
        _IsAtk = false;
        if(Target == null){
            Rigi.velocity = new Vector2(0,Rigi.velocity.y);
            _IsMoving = false;
            return;
        }

        this.transform.localScale = new Vector2(Mathf.Abs(this.transform.localScale.x) * (Target.position.x > this.transform.position.x ? 1 : -1),
                                            this.transform.localScale.y);

        Rigi.velocity = new Vector2(Speed * Time.deltaTime * (Target.position.x > this.transform.position.x ? 1:-1) , Rigi.velocity.y);
        CountDownAtk = 0;
        _IsMoving = true;
    }

    protected virtual void Attack(){
        if(CountDownAtk > 0){
            CountDownAtk -= Time.deltaTime;
            return;
        }
        CountDownAtk = 3;
        GameController.Instant.PlayerController.GetHit(HitData);
    }

    protected virtual void DefineTarget(){
        if(_IsDead)
            return;
        if(Target != null && !Target.gameObject.activeSelf){
            Target = null;
            TargetPos = this.transform.position.x;
            return;
        }

        float distance = Mathf.Abs(this.transform.position.x - GameController.Instant.PlayerController.transform.position.x);
        if(distance < RangeAtk){
            Target = GameController.Instant.PlayerController.transform;
            TargetPos = Target.position.x + 1 * (this.transform.position.x > Target.position.x ? 1 : -1);
            return;
        }

        Target = null;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(_IsDead){
            return;
        }
        SwordBase S = col.GetComponent<SwordBase>();
        if(S == null)
            return;
        if (!IsImmue )
        {
            _IsHit = true;
            _HP -= S.DataHit.Dmg;
            GameController.Instant.Pool.GetTextDmg(this.transform.position,"-"+S.DataHit.Dmg).SetActive(true);
        }
        Rigi.velocity = Vector2.zero;
        Vector2 ForceBack = S.DataHit.ForceBack;
        ForceBack.x *= (this.transform.position.x > GameController.Instant.PlayerController.transform.position.x ? 1: -1);
        Rigi.AddForce(ForceBack);
        //GetHit anim
       
        if (_HP <= 0)
        {
            _IsDead = true;
        }
    }

    void GetHitDone(){
        _IsHit = false;
    }

    void Dead()
    {
        GameObject blood = GameController.Instant.Pool.Blood;
        blood.transform.position = new Vector3(this.transform.position.x, blood.transform.position.y, blood.transform.position.z);
        blood.SetActive(true);
        Name_text.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position,RangeAtk);
    }

    enum Creep_State{
        IDLE,
        RUN,
        ATK,
        HIT,
        DIE,
        last
    }
}
