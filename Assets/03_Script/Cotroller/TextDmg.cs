﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextDmg : Animation_base
{
    [SerializeField]
    float Speed = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(this.transform.position.x ,
                                                this.transform.position.y + Speed * Time.deltaTime,
                                                this.transform.position.z);
    }
}
