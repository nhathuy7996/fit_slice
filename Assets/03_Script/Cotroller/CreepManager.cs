﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepManager : MonoBehaviour
{
    [SerializeField]
    List<Transform> Creeps = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Transform CheckCreepTarget(Vector2 Target)
    {
        foreach (Transform T in Creeps)
        {
            if (!T.gameObject.activeSelf)
                continue;
            if(Vector2.Distance(Target, T.position) <= 2)
            {
                return T;
            }
        }

        return null;
    }

    public bool IsDeadAll(){
        foreach (Transform T in Creeps)
        {
            if (T.gameObject.activeSelf)
                return false;
            
        }
        return true;
    }
}
