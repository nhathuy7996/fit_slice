﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectPooling : MonoBehaviour
{
    [SerializeField] Transform Pooling_canvas;

    List<GameObject> Hit_fxs = new List<GameObject>();

    [SerializeField]
    GameObject _Hit_fx = null;

    public GameObject Hit_fx
    {
        get
        {
            foreach (GameObject G in Hit_fxs)
            {
                if (G.activeSelf)
                    continue;
                return G;
            }

            GameObject G2 = Instantiate(_Hit_fx,this.transform.position,Quaternion.identity,this.transform);
            Hit_fxs.Add(G2);
            return G2;
        }
    }

    List<Text> TextDmgs = new List<Text>();
    [SerializeField]
    GameObject _TextDmg;

    public GameObject GetTextDmg(Vector3 pos, string text)
    {
        foreach (Text G in TextDmgs)
        {
            if (G.gameObject.activeSelf)
                continue;
            G.transform.position = Camera.main.WorldToScreenPoint(pos + Random.insideUnitSphere);
            G.text = text;
            return G.gameObject;
        }

        GameObject G2 = Instantiate(_TextDmg, this.transform.position, Quaternion.identity, Pooling_canvas);
        G2.transform.position = Camera.main.WorldToScreenPoint(pos + Random.insideUnitSphere);
        G2.GetComponent<Text>().text = text;
        TextDmgs.Add(G2.GetComponent<Text>());
        return G2;
    }

    [SerializeField]
    List<GameObject> Blood_prefab = new List<GameObject>();

    List<GameObject> Bloods = new List<GameObject>();

    public GameObject Blood
    {
        get
        {
            foreach (GameObject G in Bloods)
            {
                if (G.activeSelf)
                    continue;
                return G;
            }
            int ran = Random.Range(0,Blood_prefab.Count);
            Vector3 pos = Blood_prefab[ran].transform.position;
            GameObject G2 = Instantiate(Blood_prefab[ran], pos, Quaternion.identity, this.transform);
            
            Bloods.Add(G2);
            return G2;
        }
    }

    List<Text> CreepNames = new List<Text>();
    [SerializeField]
    GameObject CreepName_prefab;

    public GameObject GetTextNameCreep(Vector3 pos, string text)
    {
        foreach (Text G in CreepNames)
        {
            if (G.gameObject.activeSelf)
                continue;
            G.transform.position = Camera.main.WorldToScreenPoint(pos + Vector3.up);
            G.text = text;
            return G.gameObject;
        }

        GameObject G2 = Instantiate(CreepName_prefab, this.transform.position, Quaternion.identity, Pooling_canvas);
        G2.transform.position = Camera.main.WorldToScreenPoint(pos + Random.insideUnitSphere);
        G2.GetComponent<Text>().text = text;
        CreepNames.Add(G2.GetComponent<Text>());
        return G2;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
