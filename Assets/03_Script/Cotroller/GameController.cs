﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : Singleton<GameController>
{
    [SerializeField]
    CreepManager _Creep_Manager = null;
    public CreepManager Creep_Manager => _Creep_Manager;
    [SerializeField]
    PlayerController _PlayerCotroller = null;
    [SerializeField]
    Slider HP;
    public PlayerController PlayerController => _PlayerCotroller;

    [SerializeField]
    ObjectPooling _Pool = null;
    public ObjectPooling Pool => _Pool;

    [SerializeField]
    GameObject Touch_obj = null;

    [SerializeField]
    GameState GAME_STATE = GameState.PLAY;

    [SerializeField]
    GameObject RePlayButton = null;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GAME_STATE != GameState.PLAY)
            return;
        if(Input.GetMouseButtonDown(0)){
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            _PlayerCotroller.ListenClick(pos);
            if(_PlayerCotroller.CreepTarget != null){
                Touch_obj.GetComponent<Image>().color = Color.red;
            }else{
                Touch_obj.GetComponent<Image>().color = Color.white;
            }
            Touch_obj.transform.position = Input.mousePosition;
            Touch_obj.SetActive(false);
            Touch_obj.SetActive(true);
        }
        HP.value = PlayerController.HP;

        if(_Creep_Manager.IsDeadAll())
            GameOver();
    }

    public void GameOver(){
        RePlayButton.SetActive(true);
        GAME_STATE = GameState.OVER;
    }

    public void RePlay(){
        LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadScene(int index){
        SceneManager.LoadScene(index);
    }

    enum GameState{
        PLAY,
        OVER
    }
}
